using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
public class BallController : MonoBehaviour
{
    // Reference to the RigidBody Component
    Rigidbody rb;
    public float speed = 5f;
    // Used to calculate the direction from the mouse's last to current position
    Vector2 lastMousePos = Vector2.zero;
    // The force with which we can move the ball by swiping
    public float thrust = 100f;
    [SerializeField] float wallDistance = 5f;
    [SerializeField] float minCamDistance = 4f;
    public Canvas WinCanvas;
    public Canvas LoseCanvas;
    void Start()
    {
        WinCanvas.enabled = false;
        LoseCanvas.enabled = false;
        rb = GetComponent<Rigidbody>();
    }
    void FixedUpdate()
    {
        //Move the ball forward
        rb.MovePosition(rb.position + Vector3.forward * speed * Time.fixedDeltaTime);

        // Move the Camera forward
        Camera.main.transform.position += Vector3.forward * speed * Time.fixedDeltaTime;
    }
    void Update()
    {
        Vector2 deltaPos = Vector2.zero;
        if (Input.GetMouseButton(0))
        {
            Vector2 currentMousePos = Input.mousePosition;
            if (lastMousePos == Vector2.zero)
            {
                lastMousePos = currentMousePos;
            }
            deltaPos = (currentMousePos - lastMousePos);
            Vector3 force = new Vector3(deltaPos.x, 0, deltaPos.y) * thrust;
            rb.AddForce(force);
        }
        else
        {
            lastMousePos = Vector2.zero;
        }
    }
    private void LateUpdate()
    {
        Vector3 position = transform.position;
        if (position.z < Camera.main.transform.position.z + minCamDistance)
        {
            // Block the player's position
            position.z = Camera.main.transform.position.z + minCamDistance;
        }

        // Reset the position

        if (position.x < -wallDistance)
        {
            position.x = -wallDistance;
        }
        else if (position.x > wallDistance)
        {
            position.x = wallDistance;
        }

        transform.position = position;
    }
    IEnumerator Die(float delayTime)
    {
        // Do stuff before replaying the level 
        Debug.Log("You're dead");

        // Stop the Player from moving
        speed = 0;
        thrust = 0;

        // Wait some seconds
        yield return new WaitForSeconds(delayTime);

        // Do stuff after waiting some seconds

        //Replay the Level
        LoseCanvas.enabled = true;
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Evil")
        {
            StartCoroutine(Die(1));
        }
    }
    IEnumerator Win(float delayTime)
    {
        // Do stuff before waiting 
        thrust = 0;
        speed = 0;
        rb.velocity = Vector3.zero;

        //Wait some time 
        yield return new WaitForSeconds(delayTime);

        WinCanvas.enabled = true; 
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Goal")
        {
            StartCoroutine(Win(1));
        }
    }
}